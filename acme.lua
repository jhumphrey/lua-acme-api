-- acme API for lua
-- depends on 9p being installed

acme = {
	index = {},
	log = io.popen("9p read acme/log"),
	readindex	= function(self)
		self.index = {}
		local i = io.popen("9p read acme/index", "r")
		for l in i:lines() do
			local id, tl, bl, dir, mod, tag = l:match(".-(%d+).-(%d+).-(%d+).-(%d+).-(%d+)%s-(.*)")
			if id then
	self.index[id] = {taglen=tl, bodylen=bl, isdir=(dir=="1"), mod=(mod=="1"), tag=tag}
			end
		end
	end,
	createwindow	= function(self, title)
		local new = io.popen("9p write acme/new/ctl","w")
		if title then
			new:write("name "..title.."\n") 
		end
		new:close()
		self:readindex() -- TODO keep track so we can return a clean number
	end,

	gettopwindownum	= function(self)
		local currenttop = 0
		for i, w in pairs(self.index) do
			if tonumber(i) > currenttop then
				currenttop=tonumber(i)
			end
		end
		return tostring(currenttop)
	end,
	readlog		= function(self, handler)
		if not self.log then
			self.log = io.popen("9p read acme/log", "r")
		end
		-- process line
		local l = self.log:read()
		local winnum, action, tag = l:match("(%d+)%s(.-)%s(.-)")
		if not winnum then
			winnum, action = l:match("(%d+)%s(.-)")
		end
		if handler then
			return handler(winnum, action, tag, l)
		end
		return winnum, action, tag, l
	end,
	window = {
		id=0,
		body=[[]],
		selection="",
		setaddress = function(self, a1,a2)
			local f=io.popen("9p write acme/"..self.id.."/addr", "w")
			f:write(a1..","..a2)
			f:close()
		end,
		setbody=function(self)
			self:setaddress(0,0)
			local f=io.popen("9p write acme/"..self.id.."/body", "w")
			f:write(self.body)
			f:close()
		end,
		getbody=function(self)
			local f=io.popen("9p read acme/"..self.id.."/body", "r")
			self.body=f:read("*all")
			f:close()
		end,
		settag=function(self)
			local f=io.popen("9p write acme/"..self.id.."/tag", "w")
			f:write(self.tag)
			f:close()
		end,
		gettag=function(self)
			local f=io.popen("9p read acme/"..self.id.."/tag", "r")
			self.tag=f:read("*all")
			f:close()
		end,
		getsel=function(self)
			local f=io.popen("9p read acme/"..self.id.."/rdsel","r")
			self.selection=f:read("*all")
			f:close()
		end,
		readcmd =function(self, handler)
			if not self.cmdin then
				self.cmdin=io.popen("9p read acme/"..self.id.."/event","r")
			end
			
			local l = self.cmdin:read()
			local orig, act, addr1, addr2, flag, count, data = l:match("(.)(.?)(%d+)%s(%d+)%s(%d+)%s(%d+)%s?(.*)")
print("DEBUG orig is ",orig)
print("DEBUG act is ", act)
print("DEBUG addr1,2 is",addr1,addr2)
print("DEBUG flag is ",flag)
print("DEBUG count is ",count)
print("DEBUG: text is", data)
			if count == "0" then
				-- read data
				local d = io.popen("9p read acme/"..self.id.."/data", "r")
				data = d:read("*all")
				d:close()
			end
			if handler and type(handler) == "function" then
				return handler(orig, act, addr1, addr2, flag, count, data, l)
			end
			if type(handler or 0) == "table" then
				-- we have to do some work
				-- table is divided into tag and body
				local handlerfunc=false
				if orig:upper() == orig then
					handlerfunc = handler["body"]
				else
					handlerfunc = handler["tag"]
				end
				if not handlerfunc then
					handlerfunc = handler["all"]
				end
				if not handlerfunc then
					return nil, "No handler for event"
				end
				local handlerfunc2
				orig = orig:upper() 
				if orig == "E" then
					-- write event
					handlerfunc2 = handlerfunc["write"]
				elseif orig == "F" then
					-- some other window file changed
					handlerfunc2 = handlerfunc["filechange"]
				elseif orig == "K" then
					-- keyboard event
					handlerfunc2 = handlerfunc["keyevent"]
				elseif orig == "M" then
					-- mouse event
					handlerfunc2 = handlerfunc["mouseevent"]
				elseif orig == "D" then
					-- deleted text
					handlerfunc2 = handlerfunc["delete"]
				elseif orig == "I" then
					-- inserted text
					handlerfunc2 = handlerfunc["insert"]
				elseif orig == "L" then
					-- Button3 action
					handlerfunc2 = handlerfunc["button3"]
				elseif orig == "X" then
					-- Button2 action
					handlerfunc2 = handlerfunc["button2"]
				end
				return handlerfunc2(tonumber(addr1), tonumber(addr2), flag, tonumber(count), data)
			end
			return orig, addr1, addr2, flag, count, data
		end,
		ctl = function(self, command)
			local ctl = io.popen("9p write acme/"..self.id.."/ctl", "w")
			ctl:write(command.."\n")
			ctl:close()
		end,
		startclose = function(self)
			self.cmdin:close()
		end,
		error = function(self, errstr)
			local er = io.popen("9p write acme/"..self.id.."/errors", "w")
			er:write(errstr)
			er:close()
		end
	},
	windows={},
	loadwindows	= function(self)
		-- each window is indexed with the id. 
		for i, w in pairs(self.index) do -- pairs for sparse array
			acme.window.__index=acme.window
			self.windows[i] = {id=i, body=[[]],selection=""}
			setmetatable(self.windows[i], self.window)
--			self.windows[i].__index = self.window
		end
	end
}

return acme
