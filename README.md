# Lua Acme API

lua bindings for the acme(1) text editor.

# Install

copy acme.lua somewhere usefull.

# Commands/Fields
<table>
<tr><td>index</td><td>list of open acme windows</td></tr>
<tr><td>log</td><td>Log file</td></tr>
<tr><td>readindex</td><td>populates the index table with the latest index</td></tr>
<tr><td>createwindow</td><td>Open a new window</td></tr>
<tr><td>gettopwindownum</td><td>Get the newwest window. Usefull for new windows.</td></t>
<tr><td>readlog</td></tr>
<tr><Td>window</td><td>A list of windows containing the following subcommands:
<table>
<tr><td>id</td><td>The id of the window</td></tr>
<tr><td>body</td><td>The body text of the window</td></tr>
<tr><td>selection</td><td>The selection of the window</td></tr>
<tr><td>cmdin</td><td>The command file (read only)</td></tr>
<tr><td>setaddress</td><td>Set the cursor address. (see bugs)</td></tr>
<tr><td>setbody</td><td>Set the body text (see bugs)</td></tr>
<tr><td>getbody</td><td>Update window.body</td></tr>
<tr><td>settag</td><td>Set window tag</td></tr>
<tr><td>gettag</td></tr>
<tr><td>getsel</td><td>Get selection</td></tr>
<tr><td>readcmd(handler)</td><td>Get a command from the list, call handler [TODO: what is a handler?]</td></tr>
<tr><td>ctl</td><td>Send a command to the window</td></tr>
<tr><td>startclose</td><td>Not really used, supposedly closes the window</td></tr>
<tr><td>error</td><td>An error function</td>
</table></td></tr>
<tr><td>
</table>

# Bugs/Unexpected Behavior/Workarounds
When you set the body, text is appended, rather than reset.
Clear the window first, here is a (messy) snippet that just does that
```lua
yourwin:ctl("mark")
yourwin:ctl("nomark")
os.execute("echo -n , | 9p write acme/"..yourwin.id.."/data")
os.execute("cat /dev/bull | 9p write acme/"..yourwin.id.."/body")
```

The address setting function has problems, and I probably wont use it any time soon. Feel free to correct it, if you know the correct way.
# Additional Junk

I do plan on adding other files that aren't necessary, but acme related.
Instead of cloning the repository, simply grab the acme.lua file. If you find a bug, fix it and send in the patch.
